module Main where
import Language.Forth

version = "v0.0"

banner = unlines ["_|    _|    _|_|    _|_|_|    _|_|_|    _|_|_|_|  "
                 ,"_|    _|  _|    _|  _|    _|  _|    _|  _|        forth in haskell"
                 ,"_|_|_|_|  _|    _|  _|_|_|    _|    _|  _|_|_|    "++version
                 ,"_|    _|  _|    _|  _|    _|  _|    _|  _|        "
                 ,"_|    _|    _|_|    _|    _|  _|_|_|    _|_|_|_|  "
                 ]

main = do
  putStrLn banner
  forthREPL
