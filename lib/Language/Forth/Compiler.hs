-- |Interface to a Forth compiler and environment
module Language.Forth.Compiler (
  -- * Data types
  Forth,
  -- * Initialization
  initForth,
  initForth_,
  -- * Compiler functions
  compileFile,
  compileString
  -- * Example 1: Starting a @Forth@ and compiling some files
  -- $ex1

  -- * Example 2: Starting a naked @Forth@ with your own prelude
  -- $ex2
) where
import Harpy.X86CGCombinators
import Harpy.X86Assembler
import Harpy.CodeGenMonad
import Harpy.X86CodeGen
import Control.Monad.Trans
import Control.Monad
import qualified Data.Map as M
import System.FilePath
import Foreign



-- |Forth environment data structure. Contains everything
-- needed for calls to Forth later on.
data Forth = Forth {
   dictionary :: M.Map String [Word8]
}


-- |Initialize a Forth environment; this will return an
-- environment that is used later in order to
-- run files and code snippets and whatnot. 
-- 
-- This will also load the prelude that is distributed along with
-- horde, \'horde.f\'.
-- If you do not wish for this to be loaded and instead to load
-- your own prelude, (for example, restricted amounts of functions,
-- etc.) or simply have nothing available, please use 'initForth_'
initForth :: [FilePath]    -- ^ A list of files to be compiled into the environment on initialization
          -> IO Forth      -- ^ The resulting Forth environment

-- TO BE IMPLEMENTED: still needs to properly find the horde prelude, right now we
-- just assume it's in the current dir
initForth = initForth_ "./horde.f"


-- |Initializes a Forth environment, without specifying the default prelude
-- of 'horde.f', allowing you to write your own restricted or much larger
-- prelude's for the system, or simply have nothing available
initForth_ :: FilePath     -- ^ Path to a file that acts as the 'prelude,'
                           -- i.e. the main Forth kernel written in Forth.
           -> [FilePath]   -- ^ A list of files to be compiled into the environment on initialization
           -> IO Forth     -- ^ The resulting Forth environment
initForth_ prelude files = do
#ifdef DEBUG
  putStrLn $ "loading horde prelude at "++prelude++" ..."
#endif
  -- compile prelude and files here...
  return $ Forth {dictionary=M.empty}



-- |Compile an entire file into the given Forth
-- environment
compileFile :: Forth       -- ^ Forth Environment
            -> FilePath    -- ^ File to compile into environment
            -> IO Forth    -- ^ Resulting Forth environment
compileFile f _ = return f


-- |Compile a string into the given Forth environment
compileString :: Forth    -- ^ Forth Environment
              -> String   -- ^ String to compile\/execute
              -> IO Forth -- ^ Resulting Forth environment
compileString f _ = return f

{- $ex1
> import Language.Forth
> 
> main = do
>   forth <- initForth ["first.f"]
>   putStrLn "forth initialized..."
>   compileString forth "HELLO"
>   return ()

Where \'first.f\' would be simple as such:

> ( hello world! )
> : HELLO   CR ." hello world!" ;

In this case, we just have the initial prelude and
first.f compiled, which we can then manipulate very
easily like any Forth code. The result should be output like this:

> [tester@blob ~]$ ghc --make forth1.hs
> ...
> [tester@blob ~]$ ./forth1
> forth initialized...
> hello world! ok
> [tester@blob ~]$
-}

{- $ex2
In some cases it may be beneficial to your application to have
a \'limited\' Forth environment, for say, more secure evaluations.
In this case, the function 'initForth_' is allows you to specify your own
prelude:

> import Language.Forth

> main = do
>   forth <- initForth_ "myprelude.f" []
>   putStrLn "bare prelude initialized..."
>   // continue here...
-}
