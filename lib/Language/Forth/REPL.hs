-- |Forth REPL environment for interactive work
module Language.Forth.REPL (
  -- * REPL functions
  forthREPL,
  forthREPL_
  -- * Example: the entire 'horde' executable
  -- $ex
) where
import Language.Forth.Compiler
import System.FilePath
import System.IO
import Data.List

-- |Initialize a Forth REPL for entering code. There is
-- no need to initialize a 'Language.Forth.Compiler.Forth' beforehand
forthREPL :: IO ()
forthREPL = hSetBuffering stdout NoBuffering >> initForth [] >>= shell

-- |Initialize a \'naked\' Forth REPL for code evaluations. This function
-- takes a FilePath pointing to its own prelude
forthREPL_ :: FilePath -> IO ()
forthREPL_ p = hSetBuffering stdout NoBuffering >> initForth_ p [] >>= shell


shell f = getLine >>= ops f
    where 
      ops f str | "bye" == str   = putStrLn "Exiting..."
                | otherwise      = compileString f str >>= shell

{- $ex 
It is probably not suprising to know that the entire horde program -- the interpreter itself --
is simply a client of this module. For brevity and succinctness, here is the entire program
replicated to give you an idea of how simple it is:

> module Main where
> import Language.Forth
> 
> version = "v0.0"
> 
> banner = unlines ["_|    _|    _|_|    _|_|_|    _|_|_|    _|_|_|_|  "
>                  ,"_|    _|  _|    _|  _|    _|  _|    _|  _|        forth in haskell"
>                  ,"_|_|_|_|  _|    _|  _|_|_|    _|    _|  _|_|_|    "++version
>                  ,"_|    _|  _|    _|  _|    _|  _|    _|  _|        "
>                  ,"_|    _|    _|_|    _|    _|  _|_|_|    _|_|_|_|  "
>                  ]
> 
> main = do
>   putStrLn banner
>   forthREPL

There is also of course, the ability to load a forth REPL with your own prelude, 'forthREPL_'.
Ex:

> main = do
>   putStrLn welcomemsg
>   forthREPL_ "myprelude.f"
-}
